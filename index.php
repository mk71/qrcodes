<?php session_start();
# Check for the submit




$msg = '';
$msgClass = '';

    if(filter_has_var(INPUT_POST, 'submit')){
        $code = htmlspecialchars($_POST['code']);
    }

    if(!empty($code)){
        #success
        $msg="form submitted";
        $msgClass = 'alert-success';
        
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Us</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css"/>
</head>
<body>
    <nav class="nabar navbar-default">
       <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Home</a>
                <a class="navbar-brand" href="create_qr.php">Qr Codes</a>
                <a class="navbar-brand" href="course_listings.php">Courses</a>
            </div>
        </div> 
    </nav>
    <br />
    <br />
    <div class="container">
        <?php if($msg != ''): ?>
            <div class="alert <?php echo $msgClass; ?>"><?php echo $msg; ?></div>
        <?php endif; ?>
        
        <form method="post" action="create_qr.php">
            <div class="form-group">
                <label>CPD CODE</label>
                <input type="text" name="code" maxlength="20" class="form-control" value="<?php echo isset($_POST['code']) ? $code : '';?>"/>
            </div>
            <br />
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    
</body>
</html>
