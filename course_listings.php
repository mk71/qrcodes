<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);

require_once('qrlib.php');



define('EXAMPLE_TMP_URLRELPATH', "./");


$body = '';


function subs(){
    $subject = [['test1' => 'course1'], ['test2' => 'course2'], ['test3' => 'course3']];
    $temp_dir = "./";
    $email = 'cpd@ics.ie';
    $code_contents = 'mailto:'.$email;
    foreach($subject as $sub){
        foreach ($sub as $k => $v){ 
            $imgs = $k.".png";
            QRcode::png($code_contents, $temp_dir. $imgs, QR_ECLEVEL_H, 3);
            echo "<tr>\n";
            echo"<td>$k</td>\n"; 
            echo"<td>$v</td>";
            echo "<td>".'<img src="'.EXAMPLE_TMP_URLRELPATH."$imgs\" /></td>";
            echo "</tr>\n";
        }
    }
}



?>


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Us</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css"/>
</head>
<body>
    <nav class="nabar navbar-default">
       <div class="container">
            <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Home</a>
            <a class="navbar-brand" href="create_qr.php">Qr Codes</a>
            <a class="navbar-brand" href="course_listings.php">Courses</a>
            </div>
        </div> 
    </nav>
    <br /><br />
    <div class="container">
        <table class="table table-bordered table-striped">
        <thead>
            <tr>
            <th scope="col">Course Code</th>
            <th scope="col">Course Name</th>
            <th scope="col">QR Code</th>
            </tr>
        </thead>
        <tbody>
            <?php subs(); ?>
        </tbody>
        </table>
    </div>
</body>

